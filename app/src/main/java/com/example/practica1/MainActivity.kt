package com.example.practica1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Log.i("abc", "Entro al onCreate")
    }

    override fun onStart() {
        super.onStart()
        Log.i("abc", "Entro al onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.i("abc", "Entro al onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.i("abc", "Entro al onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.i("abc", "Entro al onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i("abc", "Entro al onDestroy")
    }


}